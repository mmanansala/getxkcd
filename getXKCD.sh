#!/bin/bash
if test $# -lt 2
then
   echo "Usage: ./getXKCD xdim ydim [percentage deviation]"
   exit 1
fi
dev=0.5
if test $# -ge 3
then
   dev=`echo "scale = 2; $3 / 100" | bc`
fi
ratio=`echo "scale = 2; $1 / $2" | bc`
upper=`echo "scale = 2; $ratio * (1 + $dev)" | bc`
lower=`echo "scale = 2; $ratio * (1 - $dev)" | bc`
URL=xkcd.com
numComics=`wget -q -O- $URL | grep -E '^Permanent link' | grep -E -o '[0-9]+'`
isFunny=false
while test $isFunny = false
do
   comicNum=$(( ( RANDOM % $numComics ) + 1 ))
   imageURL=`wget -q -O- "$URL/$comicNum" | grep -E '^Image URL' | cut -d: -f2- | sed -e 's/ *//g'`
   imageName=`echo "$imageURL" | rev | cut -d'/' -f1 | rev`
   wget -q -O- "$imageURL" > "/tmp/$imageName"
   dims=`file "/tmp/$imageName" | cut -d, -f2`
   xdim=`cut -dx -f1 <<< "$dims"`
   ydim=`cut -dx -f2 <<< "$dims"`
   ratio=`echo "scale = 2; $xdim / $ydim" | bc`
   if [[ `echo $ratio'<'$upper | bc -l` == 1 && `echo $ratio'>'$lower | bc -l` == 1 ]]
   then
      display "/tmp/$imageName" &
      echo "Is this sufficiently funny? (y/n)"
      read line
      if grep y <<< $line >/dev/null
      then
         mv "/tmp/$imageName" "$imageName"
         echo Image saved
         isFunny=true
      fi
   fi
done
